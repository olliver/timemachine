#!/bin/sh

if [ -e /tmp/timemachine.pid ]; then
	kill `cat /tmp/timemachine.pid`
fi
echo $$ > /tmp/timemachine.pid
killall timemachine

LOOP=0
if [ -z ${1} ]; then
	LOOP=1
fi

if [ ${LOOP} -eq 1 ]; then
	while true; do
		./timemachine idle.csv
	done
else
	./timemachine ${1}
	./timemachine.sh
fi

rm /tmp/timemachine.pid
