#/usr/bin/python3
#
# (c) Copyright 2016
# Author: Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier:  AGPL-3.0+
#

import sys

def main(argc, argv):
    f = open("idle.csv", "w")
    for hue in range(0, 256):
        for ledstrip in range(0, 25):
            f.write(str(ledstrip) + ", " + str(hue) + ", 255, 255\n")

        f.write("255, 5000, 0, 0\n")
        
    f.close()

if __name__ == "__main__":
    sys.exit(main(len(sys.argv), sys.argv))
