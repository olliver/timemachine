#/usr/bin/python3
#
# (c) Copyright 2016
# Author: Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier:  AGPL-3.0+
#

import sys

def main(argc, argv):
    f = open("forward.csv", "w")

    for ledstrip in range(0, 25):
        if ((ledstrip % 1) == 0):
            f.write(str(ledstrip) + ", 0, 0, 255\n")
        else:
            f.write(str(ledstrip) + ", 0, 0, 8\n")

        f.write("255, 10000, 0, 0\n")
    for ledstrip in range(0, 25):
        if ((ledstrip % 2) == 0):
            f.write(str(ledstrip) + ", 0, 0, 255\n")
        else:
            f.write(str(ledstrip) + ", 0, 0, 8\n")

        f.write("255, 10000, 0, 0\n")
    for ledstrip in range(0, 25):
        if ((ledstrip % 3) == 0):
            f.write(str(ledstrip) + ", 0, 0, 255\n")
        else:
            f.write(str(ledstrip) + ", 0, 0, 8\n")

        f.write("255, 10000, 0, 0\n")
        
    f.close()

if __name__ == "__main__":
    sys.exit(main(len(sys.argv), sys.argv))
