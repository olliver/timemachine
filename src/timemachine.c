/*
 * (C) Copyright 2016 Olliver Schinagl <oliver@schinagl.nl>
 *
 * SPDX-License-Identifier:	AGPL-3.0+
 *
 */

#include <csv.h>
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#ifndef DEBUG
#define LED_PATH "/sys/class/leds/pca963x:%d_%c/brightness"
#else
#define LED_PATH "/tmp/brightness_%d_%c"
#endif

#define MIN(a, b, c) ({ \
	__typeof__ (a) _a = (a); \
	__typeof__ (b) _b = (b); \
	__typeof__ (c) _c = (c); \
	_a < _b ? (_a < _c ? _a : (_b < _c ? _b : _c)) : (_b < _c ? _b : _c); \
})

struct color_rgbw {
	uint_fast8_t red;
	uint_fast8_t green;
	uint_fast8_t blue;
	uint_fast8_t white;
};

struct color_hsv {
	uint_fast8_t hue;
	uint_fast8_t sat;
	uint_fast8_t br;
};

struct csv_data {
	uint_fast8_t fields;
	uint_fast64_t rows;
	struct ledstrips *ledstrip;
	uint_fast8_t ledstripnum;
	useconds_t usleep;
	struct color_hsv color;
};

struct ledstrips {
	int fd_red;
	int fd_green;
	int fd_blue;
	int fd_white;
};

static void leds_sleep(const useconds_t sleep_time)
{
#ifndef DEBUG
	usleep(sleep_time);
#endif
}

static void hsv2rgbw(const struct color_hsv *hsv, struct color_rgbw *rgbw)
{
	uint_fast8_t angle, remainder, p, q, t;

	if (hsv->sat == 0) {
		rgbw->red = hsv->br;
		rgbw->green = hsv->br;
		rgbw->blue = hsv->br;
		rgbw->white = hsv->br;

		return;
	}

	angle = hsv->hue / 43;
	remainder = (hsv->hue - (angle * 43) ) * 6;

	p = (hsv->br * (255 - hsv->sat)) >> 8;
	q = (hsv->br * (255 - ((hsv->sat * remainder) >> 8))) >> 8;
	t = (hsv->br * (255 - ((hsv->sat * (255 - remainder)) >> 8))) >> 8;

	switch(angle) {
	case 0:
		rgbw->red = hsv->br;
		rgbw->green = t;
		rgbw->blue = p;
		break;
	case 1:
		rgbw->red = q;
		rgbw->green = hsv->br;
		rgbw->blue = p;
		break;
	case 2:
		rgbw->red = p;
		rgbw->green = hsv->br;
		rgbw->blue = t;
		break;
	case 3:
		rgbw->red = p;
		rgbw->green = q;
		rgbw->blue = hsv->br;
		break;
	case 4:
		rgbw->red = t;
		rgbw->green = p;
		rgbw->blue = hsv->br;
		break;
	default:
		rgbw->red = hsv->br;
		rgbw->green = p;
		rgbw->blue = q;
		break;
	}

	rgbw->white = MIN(rgbw->red, rgbw->green, rgbw->blue);
	rgbw->red -= rgbw->white;
	rgbw->green -= rgbw->white;
	rgbw->blue -= rgbw->white;
}

static int is_field(unsigned char c) {
	if (c == CSV_COMMA)
		return 1;

	return 0;
}

static int is_row(unsigned char c) {
	if (c == CSV_CR || c == CSV_LF)
		return 1;

	return 0;
}

void parse_fields(void *field, size_t len, void *data)
{
	struct csv_data *csv = (struct csv_data *)data;

	switch (csv->fields) {
	case 0:
		csv->ledstripnum = atoi(field);
		break;
	case 1:
		if (csv->ledstripnum == 255) {
			csv->usleep = atoi(field);
		} else {
			csv->usleep = 0;
			csv->color.hue = atoi(field);
		}
		break;
	case 2:
		csv->color.sat = atoi(field);
		break;
	case 3:
		csv->color.br = atoi(field);
		break;
	default:
		fprintf(stderr, "Error, unknown field number '%s' %d.\n", field, csv->fields);
		break;
	}
	csv->fields++;

	/* Prepare the next field for a shorter string. */
	memset(field, '\0', len);
};

static void leds_color_set(struct ledstrips *ledstrip, struct color_hsv *hsv)
{
	struct color_rgbw rgbw;

	hsv2rgbw(hsv, &rgbw);

	if (ledstrip->fd_red > 2)
		dprintf(ledstrip->fd_red, "%d\n", rgbw.red);
	if (ledstrip->fd_green > 2)
		dprintf(ledstrip->fd_green, "%d\n", rgbw.green);
	if (ledstrip->fd_blue > 2)
		dprintf(ledstrip->fd_blue, "%d\n", rgbw.blue);
	if (ledstrip->fd_white > 2)
		dprintf(ledstrip->fd_white, "%d\n", rgbw.white);
}

void parse_rows(int c, void *data)
{
	struct csv_data *csv = (struct csv_data *)data;

	if (csv->usleep)
		leds_sleep(csv->usleep);
	else
		leds_color_set(&csv->ledstrip[csv->ledstripnum], &csv->color);

	csv->rows++;
	csv->fields = 0;
}

#define BUF_SIZE 1024
static int leds_run(struct ledstrips ledstrip[], const char *csv_filename)
{
	uint_fast16_t i;
	uint_fast8_t hue, sat, br;
	struct csv_parser csv;
	FILE *csv_file;
	size_t bytes_read = 0;
	char buf[BUF_SIZE] = { 0 };
	struct csv_data data;

	csv_file = fopen(csv_filename, "rb");
	if (!csv_file) {
		fprintf(stderr, "Failed to open file '%s'. %s.\n", csv_filename, strerror(errno));
		return -ENOSYS;
	}

	csv_init(&csv, 0);
	csv_set_space_func(&csv, is_field);
	csv_set_term_func(&csv, is_row);

	data.ledstrip = ledstrip;
	while ((bytes_read = fread(buf, 1, BUF_SIZE, csv_file)) > 0) {
		if (csv_parse(&csv, buf, bytes_read, parse_fields, parse_rows, &data) != bytes_read) {
			fprintf(stderr, "Error parsing csv file %s.", csv_strerror(csv_error(&csv)));
		}
	}
	csv_fini(&csv, &parse_fields, &parse_rows, &data);
	if (ferror(csv_file)) {
		fprintf(stderr, "Error while reading file %s.\n", csv_filename);
	}
	fclose(csv_file);
	csv_free(&csv);

	printf("CSV data from filename: %s\n", csv_filename);
	printf("Parsed %lld rows.\n", data.rows);

	csv_free(&csv);

	return 0;
}

int leds_open(char *path)
{
	int fd = -1;

#ifndef DEBUG
	fd = open(path, O_WRONLY);
#else
	fd = open(path, O_WRONLY | O_CREAT | O_TRUNC,
		 S_IRWXU | S_IRWXG | S_IRWXO);
#endif
	if (fd < 0)
		fprintf(stderr, "Unable to open %s\n", path);

	return fd;
}

static int leds_init(struct ledstrips ledstrip[])
{
	char buf[sizeof(LED_PATH) + 4] = { 0 };
	uint_fast16_t i = 0;
	int fd_err = 0;

	do {
		snprintf(buf, sizeof(LED_PATH) + 4, LED_PATH, i, 'r');
		ledstrip[i].fd_red = leds_open(buf);
		if (ledstrip[i].fd_red < 0)
			fd_err++;

		snprintf(buf, sizeof(LED_PATH) + 4, LED_PATH, i, 'g');
		ledstrip[i].fd_green = leds_open(buf);
		if (ledstrip[i].fd_green < 0)
			fd_err++;

		snprintf(buf, sizeof(LED_PATH) + 4, LED_PATH, i, 'b');
		ledstrip[i].fd_blue = leds_open(buf);
		if (ledstrip[i].fd_blue < 0)
			fd_err++;

		snprintf(buf, sizeof(LED_PATH) + 4, LED_PATH, i, 'w');
		ledstrip[i].fd_white = leds_open(buf);
		if (ledstrip[i].fd_white < 0)
			fd_err++;

		i++;
#ifndef DEBUG
	} while (!fd_err);
#else
	} while (i < 24);
#endif

	return 0;
}

static int leds_shutdown(struct ledstrips ledstrip[])
{
	uint_fast16_t i;

	for (i = 0; i < 255; i++) {
		if (ledstrip->fd_red > 2)
			close(ledstrip[i].fd_red);
		if (ledstrip->fd_green > 2)
			close(ledstrip[i].fd_green);
		if (ledstrip->fd_blue > 2)
			close(ledstrip[i].fd_blue);
		if (ledstrip->fd_white > 2)
			close(ledstrip[i].fd_white);
	}

	return 0;
}

int main(int argc, char *argv[])
{
	struct ledstrips ledstrip[255] = { 0 };
	FILE csv_file;

	if (argc < 2) {
		fprintf(stderr,
			"Run as %s <csvfilename>\n",
			argv[0]);
		exit(EXIT_FAILURE);
	}

	leds_init(ledstrip);

	leds_run(ledstrip, argv[1]);

	leds_shutdown(ledstrip);

	return 0;
}
